# Copyright (C) Alberto Pianon <alberto@pianon.eu>
#
# SPDX-License-Identifier: GPL-2.0-only
#

import os
import re
import tempfile
import subprocess
from pathlib import Path


def init(d, task):
    """Get or create temporary directory and git repo to trace upstream sources during task execution"""

    tmpdir = d.getVar("__SRCTRACE_TMPDIR_%s" % task)

    if not tmpdir:
        tmpdir = tempfile.mkdtemp(dir=d.getVar("WORKDIR"))
        d.setVar("__SRCTRACE_TMPDIR_%s" % task, tmpdir)

    bare = d.getVar("__SRCTRACE_BAREGIT_%s" % task)
    
    if not bare:
        bare = os.path.join(d.getVar("WORKDIR"), "srctrace_%s.git" % task)
        d.setVar("__SRCTRACE_BAREGIT_%s" % task, bare)
        if os.path.exists(bare):
            subprocess.check_output("rm -Rf %s" % bare, shell=True)
        subprocess.check_output("git init --bare %s" % bare, cwd=tmpdir, shell=True)
        gitcmd = "git --git-dir=%s --work-tree=%s" % (bare, tmpdir)
        subprocess.check_output("%s checkout -b srctrace" % gitcmd, cwd=tmpdir, shell=True)

    return tmpdir


def commit(d, task, comment):
    """Add and commit added/changed files
    
    Some notes: 
    
    - comment may contain source tracing metadata and should be unique in the 
      context of a task, to check duplicates
    
    - need to force git to ignore .git subdirs and .gitignore files (otherwise
      it would not correctly trace sources when multiple upstream sources are 
      combined together in the same directory)
    """ 

    tmpdir = d.getVar("__SRCTRACE_TMPDIR_%s" % task)
    bare = d.getVar("__SRCTRACE_BAREGIT_%s" % task)
    done = d.getVar("__SRCTRACE_DONE_%s" % task) or set()

    gitcmd = "git --git-dir=%s --work-tree=%s" % (bare, tmpdir)
    
    # apparently there is no other clean way to force git to ignore '.git' subdirs
    # than renaming them
    git_renames = {}
    git_renames.update({ 
        str(git_dir) : re.sub(".git$", ".pseudogit", str(git_dir))
        for git_dir in Path(tmpdir).rglob('.git') 
    })
    git_renames.update({
        str(gitignore): re.sub(".gitignore$", ".pseudogitignore", str(gitignore))
        for gitignore in Path(tmpdir).rglob('.gitignore')
    })

    for git, pseudogit in git_renames.items():
        os.rename(git, pseudogit)
    
    subprocess.check_output("%s add -- . ':!*/.pseudogit/*' ':!*/.pseudogitignore'" % gitcmd, cwd=tmpdir, shell=True)
    try:
        subprocess.check_output("%s commit -m '%s'" % (gitcmd, comment), cwd=tmpdir, shell=True)
    except subprocess.CalledProcessError as e:
        # do not raise exception in case of duplicate src_uris in do_unpack: 
        # there is nothing to commit the second time the same src_uri is unpacked
        # so git commit returns error
        if comment not in done: 
            raise e
    
    for git, pseudogit in git_renames.items():
        os.rename(pseudogit, git)
    
    done.add(comment)
    d.setVar("__SRCTRACE_DONE_%s" % task, done)


def move2root(d, task, root):
    """move sources to from temporary directory to root (workdir)"""

    tmpdir = d.getVar("__SRCTRACE_TMPDIR_%s" % task)
    for f in os.listdir(tmpdir):
        src = os.path.join(tmpdir, f)
        dest = os.path.join(root, f)
        if os.path.isdir(dest):
            subprocess.check_output("rm -Rf %s" % dest, shell=True)
        os.rename(src, dest)
    os.rmdir(tmpdir)
    d.setVar("__SRCTRACE_TMPDIR_%s" % task, None)
    d.setVar("__SRCTRACE_BAREGIT_%s" % task, None)
    d.setVar("__SRCTRACE_DONE_%s" % task, None)

